<?php

//class to handle addresses
class mailing_address{

	private $line1;
	private $line2;
	private $line3;
	private $line4;
	private $line5;
	private $outputStr;

	public function __construct($one, $two, $three, $four, $five){
		$this->line1=$one;
		$this->line2=$two;
		$this->line3=$three;
		$this->line4=$four;
		$this->line5=$five;
	}

	public function set_Address(){
		global $mysqli;

		$stmt = $mysqli->prepare("INSERT INTO mailingAddress (
		mailing1,
		mailing2,
		mailing3,
		mailing4,
		mailing5
		)
		VALUES (
		?,
		?,
		?,
		?,
		?
		)");

		$stmt->bind_param("sssss", $this->line1, $this->line2, $this->line3, $this->line4, $this->line5);
		$stmt->execute();
		$inserted_id = $mysqli->insert_id;
		$stmt->close();

		return $inserted_id;
	}

	// get addressID of current object
	public function get_AddressID(){
		global $mysqli;

		$result = $mysqli->query("
			SELECT addressID 
			FROM mailingAddress 
			WHERE mailing1='".$this->line1."' 
			AND mailing2='".$this->line2."' 
			AND mailing3='".$this->line3."' 
			AND mailing4='".$this->line4."' 
			AND mailing5='".$this->line5."'
			");
				
		while($row = $result->fetch_assoc()){
			$id=$row["addressID"];
		}
		$result->close();

		return $id;	
	}

	// create html string of invitation addressing information and QR code
	public static function get_InvitationInfo($mode='verbose'){
		global $mysqli;

		$outputStr="";

		$result = $mysqli->query("
			SELECT guestList.qrKey, mailingAddress.mailing1, mailingAddress.mailing2, mailingAddress.mailing3, mailingAddress.mailing4, mailingAddress.mailing5, mailingAddress.addressID  
			FROM mailingAddress, guestList 
			WHERE mailingAddress.addressID=guestList.addressID 
			GROUP BY mailing1, mailing2, mailing3, mailing4, mailing5
			");

		while($row = $result->fetch_assoc()){

			$subresult = $mysqli->query("
				SELECT guestList.title, guestList.firstName, guestList.middleName, guestList.lastName
				FROM guestList
				WHERE guestList.addressID=".$row["addressID"]."
				");

			$outputStr=$outputStr."<div style='border: 1px solid black; overflow: auto;'><div style='float:left;'>";		
			$outputStr=$outputStr."<img src='invitationQR/".$row["qrKey"].".png'><br>".$row["qrKey"];
			$outputStr=$outputStr."</div><div style='float:left;'>";

			$data[]=$row["qrKey"];

			$titles = array();
			$lastNames = array();
			$firstNames = array();
			$middleNames = array();
			$uniqueLastNames = array();
			$lastName="";

			// determine naming by looking at total number of people per address and unique last names
			switch($subresult->num_rows){
				case 0:
					break;
				case 1:
					while($subrow = $subresult->fetch_assoc()){
						$outputStr=$outputStr.$subrow["title"]." ".$subrow["firstName"]." ".$subrow["middleName"]." ".$subrow["lastName"]." and Guest<br>";
					}
					break;
				case 2:
					$ct=0;
					while($subrow = $subresult->fetch_assoc()){
						$titles[$ct]=$subrow["title"];
						$firstNames[$ct]=$subrow["firstName"];
						$middleNames[$ct]=$subrow["middleName"];
						$lastNames[$ct]=$subrow["lastName"];
						$uniqueLastNames[$subrow["lastName"]]=1;
						$lastName=$subrow["lastName"];
						$ct++;
					}

					switch(count($uniqueLastNames)){
						case 1:
							if($titles[0]=="Mr."){
								//$nameSTR=$titles[0]." ".$firstNames[0]." ".$middleNames[0]." and ".$titles[1]." ".$firstNames[1]." ".$middleNames[1]." ".$lastName;
								$nameSTR=$titles[0]." and ".$titles[1]." ".$firstNames[0]." ".$middleNames[1]." ".$lastName;
							}else{
								$nameSTR=$titles[1]." and ".$titles[0]." ".$firstNames[1]." ".$middleNames[1]." ".$lastName;
							}
							$outputStr=$outputStr.$nameSTR."<br>";
							break;
						default:
							$nameSTR="";
							foreach($firstNames as $key => $value){
								$nameSTR=$nameSTR.$titles[$key]." ".$firstNames[$key]." ".$middleNames[$key]." ".$lastNames[$key]."<br>";
							}

							$outputStr=$outputStr.$nameSTR;
							break;
					}
					break;
				default:
					$ct=0;
					while($subrow = $subresult->fetch_assoc()){
						$titles[$ct]+$subrow["title"];
						$firstNames[$ct]=$subrow["firstName"];
						$middleNames[$ct]=$subrow["middleName"];
						$lastNames[$ct]=$subrow["lastName"];
						$uniqueLastNames[$subrow["lastName"]]=1;
						$lastName=$subrow["lastName"];
						$ct++;
					}

					switch(count($uniqueLastNames)){
						case 1:
							$outputStr=$outputStr.$lastName." Family<br>";
							break;
						default:
							$nameSTR="";
							foreach($firstNames as $key => $value){
								$nameSTR=$nameSTR.$titles[$key]." ".$firstNames[$key]." ".$middleNames[$key]." ".$lastNames[$key]."<br>";
							}

							$outputStr=$outputStr.$nameSTR;
							break;
					}
					break;
			}
			$subresult->close();

			$outputStr=$outputStr.$row["mailing1"]."<br>".$row["mailing2"]."<br>".$row["mailing3"]."<br>".$row["mailing4"]."<br>".$row["mailing5"];

			$outputStr=$outputStr."</div></div>";
			
		}
		$result->close();

		switch($mode):
			case 'verbose': return $outputStr;
			case 'data':	return $data;
		endswitch;
	}
}