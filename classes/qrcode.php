<?php

//class to handle QR codes
class QR_code{

	private $fileName;
	private $filePath;
	private $QRstring;
	private $QRkey;

	public function __construct(){
		$this->filePath="invitationQR";

		global $mysqli;

		$validKey=0;
		$unique="";

		// keep looping until a valid key is found
		while($validKey==0){
			// create unique key for qr code
			$unique=md5(uniqid(rand(), true));
			$unique=substr($unique,0,5);

			// check if key is unique
			$result = $mysqli->query("
				SELECT * 
				FROM guestList 
				WHERE qrKey='".$unique."' 
			");

			if($result->num_rows==0){
				$validKey=1;
			}
				
			$result->close();
		}

		$this->QRkey=$unique;
		//$this->QRstring="http://www.arjunandlauren.com/rsvp/respond.php?key=".$this->QRkey;
		$this->QRstring="http://sn.im/2a23eu2?key=".$this->QRkey;
		$this->fileName=$this->filePath."/".$unique.".png";

		//arguments: url, filename, ECC (L,M,Q,H), size, border)
		// good read on ECC values:
		// http://www.qrstuff.com/blog/2011/12/14/qr-code-error-correction
		QRcode::png($this->QRstring,$this->fileName,'L', 10, 0);
	}

	public function get_QRkey(){
		return $this->QRkey;
	}
}