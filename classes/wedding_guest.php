<?php

//class to handle wedding guests
class wedding_guest {
	private $title;
	private $firstName;
	private $middleName;
	private $lastName;
	private $addressID;
	private $qrCode;
	private $guestID;

	public function __construct($ttl, $first, $middle, $last, $aid){
		$this->title=$ttl;
		$this->firstName=$first;
		$this->middleName=$middle;
		$this->lastName=$last;

		$this->addressID=$aid;
	}

	public function createQRCode(){
		$this->qrCode = new QR_code();
	}

	public function set_guest($mode='create',$id=null){
		global $mysqli;

		if($mode=='create'){
			$stmt = $mysqli->prepare("INSERT INTO guestList (title,
			firstName,
			middleName,
			lastName,
			addressID,
			qrKey
			)
			VALUES (
			?,
			?,
			?,
			?,
			?,
			?
			)");

		$stmt->bind_param("ssssis",
			$this->title,
			$this->firstName,
			$this->middleName,
			$this->lastName,
			$this->addressID,
			$this->qrCode->get_QRkey()
			);

		}else{
			$stmt = $mysqli->prepare("UPDATE guestList SET
			title=?,
			firstName=?,
			middleName=?,
			lastName=?
			WHERE
			guestID=?
			");

			$stmt->bind_param("sssss",
			$this->title,
			$this->firstName,
			$this->middleName,
			$this->lastName,
			$id
			);
		}

		$stmt->execute();
		echo $stmt->error;

		if($mode=='create'){
			$inserted_id = $mysqli->insert_id;
			$this->guestID=$inserted_id;
		}else{
			$this->guestID=$id;
		}

		$stmt->close();

		return $inserted_id;
	}

	public function getForm($guestID){
		global $mysqli;
		$this->guestID=$guestID;

		$sql="SELECT * FROM guestList, mailingAddress WHERE guestList.addressID=mailingAddress.addressID AND guestList.guestID=".$this->guestID;

		$result = $mysqli->query($sql);

		while($row = $result->fetch_assoc()){
			$this->title=$row['title'];
			$this->firstName=$row['firstName'];
			$this->middleName=$row['middleName'];
			$this->lastName=$row['lastName'];
			$this->addressID=$row['addressID'];
			$this->qrCode=$row['qrKey'];
			$mail1=$row['mailing1'];
			$mail2=$row['mailing2'];
			$mail3=$row['mailing3'];
			$mail4=$row['mailing4'];
			$mail5=$row['mailing5'];
		}

		echo "<form method='post'>";

		echo "<label>Title:</label>";

		wedding_guest::getTitles($this->title);

		echo "<br>";
		echo "<label>First Name:</label><input name='firstName' value='".$this->firstName."'><br>";
		echo "<label>Middle Name:</label><input name='middleName' value='".$this->middleName."'><br>";
		echo "<label>Last Name:</label><input name='lastName' value='".$this->lastName."'><br>";
		echo "<input type=hidden name='guestID' value=".$guestID.">";

		echo "<p>$mail1<br>$mail2<br>$mail3<br>$mail4<br>$mail5</p>";

		echo "invited to:<br>";
		wedding_guest::getInvitationProperties('invitation',$guestID);
		echo "<br>properties of guest:<br>";
		wedding_guest::getInvitationProperties('general',$guestID);

		echo "<br>		<input type='submit'>";

		echo "</form>";
	}

	public function clearProperties(){
		global $mysqli;

		$stmt = $mysqli->prepare("DELETE FROM guestProperty WHERE guestID=?");
		$stmt->bind_param('i',$this->guestID);
		$stmt->execute();
		$stmt->close();
	}

	public function setProperty($propertyID){
		global $mysqli;

		$stmt = $mysqli->prepare("INSERT INTO guestProperty (propertyID, guestID) VALUES (?,?)");

		$stmt->bind_param("ii",$propertyID,$this->guestID);
		$stmt->execute();
		echo $stmt->error;
		$stmt->close();
	}

	public function getIDFromQR($scannedQRCode){
		global $mysqli;

		$this->qrCode=$scannedQRCode;

		$sql="SELECT * FROM `guestList` WHERE `qrKey`='".$scannedQRCode."'";
		$result = $mysqli->query($sql);

		while($row = $result->fetch_assoc()){
			$this->guestID=$row['guestID'];
			$this->title=$row['title'];
			$this->firstName=$row['firstName'];
			$this->middleName=$row['middleName'];
			$this->lastName=$row['lastName'];
			$this->addressID=$row['addressID'];
		}
	}

	public function getProperty($propertyID, $guestID){
		global $mysqli;
		$output = array();

		$sql="SELECT * FROM `guestProperty` WHERE `propertyID`=".$propertyID." AND `guestID`=".$guestID;
		$result = $mysqli->query($sql);

		if($result->num_rows>0){
			$sql="SELECT propertyName FROM propertyList WHERE propertyID=".$propertyID;
			$response = $mysqli->query($sql);

			while($row = $response->fetch_assoc()){
				$output[] = $row['propertyName'];
			}
		}

		$checked="";

		$sql = "SELECT * FROM response WHERE guestID='".$guestID."' AND eventID='".$propertyID."'";
		$checkStatus = $mysqli->query($sql);
		if($checkStatus->num_rows>0)$checked=" checked ";

		foreach($output as $index => $propertyName){
			echo "<input type='checkbox' id='guest".$guestID."' name='".$guestID."[]' value='".$propertyID."'".$checked."><label>".$propertyName."</label><br>";
		}
	}

	public function printGuestAtt(){
		global $mysqli;

		$checkno="";
		$checkyes="";

		$sql = "SELECT * FROM response WHERE guestID=".$this->guestID." AND eventID=12";
		$result = $mysqli->query($sql);
		if($result->num_rows>0){
			$checkno=" checked ";
		}

		$sql = "SELECT * FROM response WHERE guestID=".$this->guestID." AND eventID=14";
		$result = $mysqli->query($sql);
		if($result->num_rows>0){
			$checkyes=" checked ";
		}

		echo "<input type='hidden' name='guestID[]' value='$this->guestID'>";
		echo "<div class='subItem' style='margin-left: 10px;'>";
		echo "<b>$this->title $this->firstName $this->middleName $this->lastName</b><br>";
		echo "<input type='radio' id='".$this->guestID."yes'name='".$this->guestID."[]' value='14' onclick=showOptions('".$this->guestID."'); ".$checkyes."><label>Attending</label>";
		echo "<input type='radio' name='".$this->guestID."[]' value='12' onclick=hideOptions('".$this->guestID."'); ".$checkno."><label>Not Attending</label><br>";
		echo "</div>";
	}

	/**
	* enter a response into response table
	**/
	public static function respondRSVP($guestID, $eventID){
		global $mysqli;

		$stmt = $mysqli->prepare("INSERT INTO response (guestID, eventID) VALUES (?,?)");

		$stmt->bind_param("ii",$guestID,$eventID);
		$stmt->execute();
		$stmt->close();
	}

	/**
	* save music suggestion
	**/
	public static function saveSong($guestID, $songName){
		global $mysqli;

		$stmt = $mysqli->prepare("INSERT INTO music (guestID, songTitle) VALUES (?,?)");

		$stmt->bind_param("is",$guestID,$songName);
		$stmt->execute();
		$stmt->close();
	}

	public static function clearSong($guestID){
		global $mysqli;

		$stmt = $mysqli->prepare("DELETE FROM music WHERE guestID=?");

		$stmt->bind_param("i",$guestID);
		$stmt->execute();
		$stmt->close();
	}

	public static function clearRSVP($guestID){
		global $mysqli;

		$stmt = $mysqli->prepare("DELETE FROM response WHERE guestID=?");

		$stmt->bind_param("i",$guestID);
		$stmt->execute();
		$stmt->close();
	}

	public function getRSVPForm(){
		if( $this->addressID != "" ){
			echo "<p class='midheading'>Hello $this->firstName! Thank you for using RSVPr.<br><br>";
			echo "Please kindly respond below:</p>";

			echo "<form method='get'>";
			echo "<input type='hidden' name='key' value='".$_GET['key']."'>";

			// print primary guest on invitation
			$this->printGuestAtt();

			global $mysqli;

			$sql="SELECT * FROM `guestList` WHERE `addressID`=".$this->addressID;

			$result = $mysqli->query($sql);

			echo "<div id='heading".$this->guestID."' style='display: none;' class='smallheading'>";
			echo "<b>Please select all that apply to ".$this->firstName.":</b>";
			echo "<div id='".$this->guestID."' style='display: none;'>";
			$this->getProperty(8,$this->guestID);
			$this->getProperty(9,$this->guestID);
			$this->getProperty(10,$this->guestID);
			echo "</div>";
			echo "</div>";

			echo "<script type='text/javascript'>";
			echo "checkOptions('".$this->guestID."');";
			echo "</script>";

			// print associated guests on invitation
			while($row = $result->fetch_assoc()){
				if($row['guestID']==$this->guestID)continue;

				$associatedGuest = new wedding_guest($row['title'], $row['firstName'], $row['middleName'], $row['lastName'], $row['addressID']);
				$associatedGuest->guestID=$row['guestID'];
				$associatedGuest->printGuestAtt();

				echo "<div id='heading".$associatedGuest->guestID."' style='display: none;' class='smallheading'>";
				echo "<b>Please select all that apply to ".$associatedGuest->firstName.":</b>";
				echo "<div id='".$associatedGuest->guestID."' style='display: none;'>";
				$this->getProperty(8,$associatedGuest->guestID);
				$this->getProperty(9,$associatedGuest->guestID);
				$this->getProperty(10,$associatedGuest->guestID);
				echo "</div>";
				echo "</div>";

				echo "<script type='text/javascript'>";
				echo "checkOptions('".$associatedGuest->guestID."');";
				echo "</script>";
			}

			// print music question on invitation
			$sql = "SELECT * FROM `music` WHERE `guestID`=".$this->guestID;
			$result = $mysqli->query($sql);
			while( $row = $result->fetch_assoc() ){
				$songName = $row['songTitle'];
			}

			echo "<div class='subItem' style='margin-left: 10px;'>";
			echo "<b>Help us pick music!</b> We want to hear what you want to hear.<br>";
			echo "<input type='text' name='song' id='song' style='width: 80%;' value='".htmlspecialchars($songName)."' placeholder='Your favorite dance song here'></input>";
			echo "</div>";

			echo "<br>";
			echo "<input type='submit' value='Respond to RSVP' class='myButton'>";
			echo "</form>";
		}else{
			echo "Invalid response code<br>";
			echo "<button onclick=window.open('respond.php','_self')>Try Again</button>";
		}
	}

	public static function getInvitationProperties($type,$id=null){
		switch($type){
			case "invitation":
				$inv=1;
				$name="invitation_properties";
				break;
			default:
				$name="general_properties";
				$inv=0;
				break;
		}

		global $mysqli;

		$selected=array();

		if($id!=null){
			$sql="SELECT propertyID FROM guestProperty WHERE guestID=".$id;

			$result = $mysqli->query($sql);

			while($row = $result->fetch_assoc()){
				$selected[$row['propertyID']]=1;
			}
		}

		$sql="SELECT
		propertyID,
		propertyName
		FROM
		propertyList
		WHERE
		invitation=".$inv
		;

		$result = $mysqli->query($sql);

		echo "		<select name='".$name."[]' size='".mysqli_num_rows($result)."' multiple='multiple'>";

		while($row = $result->fetch_assoc()){
			if($selected[$row['propertyID']]==1){
				echo "			<option value='".$row['propertyID']."' selected>".$row['propertyName']."</option>";
			}else{
				echo "			<option value='".$row['propertyID']."'>".$row['propertyName']."</option>";
			}
		}

		echo "</select>";

	}

	public static function getTitles($selected){
		global $mysqli;

		$sql="SELECT titleName FROM titles";

		$result = $mysqli->query($sql);

		echo "<select id='title' name='title'>";
		echo "<option></option>";

		while($row = $result->fetch_assoc()){
			if($selected==$row['titleName']){
				print("		<option value=".$row['titleName']." selected>".$row['titleName']."</option>");
			}else{
				print("		<option value=".$row['titleName'].">".$row['titleName']."</option>");
			}
		}

		echo "</select>";

	}

	public static function getAllGuestAddresses($key='address'){
		global $mysqli;

		$sql="SELECT
		guestList.guestID,
		guestList.addressID,
		guestList.firstName,
		guestList.lastName,
		mailingAddress.mailing1,
		mailingAddress.mailing2,
		guestList.addressID
		FROM
		guestList,
		mailingAddress
		WHERE
		guestList.addressID=mailingAddress.addressID
		";

		$result = $mysqli->query($sql);

		while($row = $result->fetch_assoc()){
			if($key == 'guest'){
				print("				<option value='".$row['guestID']."'>".$row['firstName']." ".$row['lastName']." - ".$row['mailing1']." ".$row['mailing2']."</option>\n");
			} else {
				print("				<option value='".$row['addressID']."'>".$row['firstName']." ".$row['lastName']." - ".$row['mailing1']." ".$row['mailing2']."</option>\n");
			}
		}

		$result->close();
	}

	public static function getDelinquents(){
		global $mysqli;

		$sql="SELECT * FROM `response`, `guestList`, `propertyList` WHERE response.guestID=guestList.guestID AND response.eventID=propertyList.propertyID";

		$result = $mysqli->query($sql);
		$guestCount = 0;
		$prevGuest = "";
		$attending = Array();

		while($row = $result->fetch_assoc()){
			if ( $row['guestID'] != $prevGuest ) {
				$attending[$row['guestID']]=1;
			}
		}

		$sql="SELECT * FROM `guestList`";

		$result = $mysqli->query($sql);

		while($row = $result->fetch_assoc()){
			if( $attending[$row['guestID']] != 1 ){
				$guestCount++;
				//print $row['guestID'];
				echo " (".$row['qrKey'].") ".$row['firstName']." ".$row['lastName'];
				print "<br>";
			}
		}

		print '<br><h2>total delinquents: '.$guestCount.'</h2>';
	}

	public static function getResponses(){
		global $mysqli;

		$sql="SELECT * FROM `response`, `guestList`, `propertyList` WHERE response.guestID=guestList.guestID AND response.eventID=propertyList.propertyID";

		$result = $mysqli->query($sql);

		$guestCount = 2;

		$prevGuest = "";

		echo "<p>";

		while($row = $result->fetch_assoc()){
			if ( $row['guestID'] != $prevGuest ) {
				echo "</p><p>";

				$subSql = "SELECT * FROM music WHERE guestID=".$row['guestID'];
				$subResult = $mysqli->query($subSql);

				while($subRow = $subResult->fetch_assoc()){
					echo "Guest suggested song '".$subRow['songTitle']."'<br>";
				}

				echo " (".$row['qrKey'].") ".$row['firstName']." ".$row['lastName'];
				$prevGuest = $row['guestID'];

			}

			switch( $row['propertyID'] ){
				case 14:
					echo " -- Attending";
					$guestCount++;
					break;
				case 12:
					echo " -- <span style='color: #f00'>Not Attending</span>";
					break;
				case 8:
					echo $row['propertyName'];
					$guestCount++;
					break;
				default:
					echo $row['propertyName'];
					break;
			}
			echo "<br>";
		}

		echo "<p><h2>Total Head Count: ".$guestCount."</h2></p>";
	}
}
