<?php
require("phpqrcode/qrlib.php");
require("db-settings.php");
require("classes/qrcode.php");
require("classes/mailing_address.php");
require("classes/wedding_guest.php");

putenv('GDFONTPATH=' . realpath('.'));
$font = 'MoolBoran';

foreach(mailing_address::get_InvitationInfo('data') as $key => $qrcode){

	if( !file_exists('genInvitation/'.$qrcode.'_merged.png') ){

		// load base image
		$invitationImage = imagecreatefrompng('source_img/design11.png');  //4800x6300
		// load qr code
		$qrImage = imagecreatefrompng('invitationQR/'.$qrcode.'.png'); //250x250
		// rotate qr code to match base image
		$qrImage = imagerotate($qrImage, 90, 0);

		// create new merged image
		$merged_image = imagecreatetruecolor(4800, 6300);

		// create colors
		$black = imagecolorallocate($merged_image, 0, 0, 0);
		$white = imagecolorallocate($merged_image, 255, 255, 255);

		/*
		$red = imagecolorallocate($merged_image, 255, 0, 0);

		//make image background red
		imagefill($merged_image, 0, 0, $red);

		//make red transparent
		imagecolortransparent($merged_image, $red);
		*/

		// add base image to merged image
		imagecopymerge($merged_image, $invitationImage, 0, 0, 0, 0, 4800, 6300, 100);
		// add qr code to merged image
		imagecopymerge($merged_image, $qrImage, 3563, 647, 0, 0, 250, 250, 100);

		// add text to merged image
		imagettftext($merged_image, 72, 90, 3871, 852, $black, $font, $qrcode);
		
		// save image to file
		header('Content-Type: image/png');
		imagepng($merged_image, 'genInvitation/'.$qrcode.'_merged.png');
	}
}