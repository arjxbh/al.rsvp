<?php
require("phpqrcode/qrlib.php");
require("db-settings.php");
require("classes/qrcode.php");
require("classes/mailing_address.php");
require("classes/wedding_guest.php");
?>

<?php
// check if form has been posted
if (isset($_POST['firstName'])){
 	$guest = new wedding_guest($_POST['title'],
 		$_POST['firstName'],
 		$_POST['middleName'],
 		$_POST['lastName'],
 		''
 		);

 	$result=$guest->set_guest('update',$_POST['guestID']);

 	$guest->clearProperties();

 	foreach ($_POST['invitation_properties'] as $selectedOption)
 		$guest->setProperty($selectedOption);

 	foreach ($_POST['general_properties'] as $selectedOption)
 		$guest->setProperty($selectedOption);
}
?>

<html>
	<head>
		<style type="text/css">
		label {
			width: 250px;
			font-weight: bold;
			display:inline-block;
		}
		</style>
	</head>
	<body>

	<form method="post">
		Select Guest to Edit:
		<select name="guestID" onchange="this.form.submit();">
			<option></option>
<?php
wedding_guest::getAllGuestAddresses('guest');
?>
		</select>
	</form>

<?php
if($_POST['guestID']){
	echo "editing guest ID: ".$_POST['guestID'];

	echo "<form method='post'>";
	$prev=$_POST['guestID']-1;
	echo "<input type='hidden' name='guestID' value='".$prev."'>";
	echo "<input type='submit' value='&lt; prev'>";
	echo "</form>";

	echo "<form method='post'>";
	$next=$_POST['guestID']+1;
	echo "<input type='hidden' name='guestID' value='".$next."'>";
	echo "<input type='submit' value='next &gt;'>";
	echo "</form>";
}

$guestForm = new wedding_guest('','','','','');
$guestForm->getForm($_POST['guestID']);
?>
	</body>
</html>