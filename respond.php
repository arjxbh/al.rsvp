<?php
require("phpqrcode/qrlib.php");
require("db-settings.php");
require("classes/qrcode.php");
require("classes/mailing_address.php");
require("classes/wedding_guest.php");
?>

<html>
	<head>
		<meta name="viewport" content="width=device-width" />
		<!-- <meta name="viewport" content="initial-scale=1.0, width=device-width, target-densitydpi=high-dpi" /> -->
		<link rel="stylesheet" href="fontawesome/css/font-awesome.css">

		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="css/bootstrap.min.css">

		<!-- Optional theme -->
		<link rel="stylesheet" href="css/bootstrap-theme.min.css">

		<!-- Latest compiled and minified JavaScript -->
		<script src="js/jquery-1.11.3.min.js"></script>
		<script src="js/bootstrap.min.js"></script>

		<script type="text/javascript">
			function showOptions(guestID){
				if(document.getElementById(guestID).innerHTML!=""){
					document.getElementById(guestID).style.display='';
					document.getElementById('heading'+guestID).style.display='';
				}
			}
			function hideOptions(guestID){
				document.getElementById(guestID).style.display='none';
				document.getElementById('heading'+guestID).style.display='none';
				var opts=document.getElementsByName(guestID+'[]');
				for(var i=0; i<opts.length; i++){
					if(opts[i].type == "checkbox")opts[i].checked=false;
				}
			}
			function checkOptions(guestID){
				if(document.getElementById(guestID+'yes').checked==true){
					showOptions(guestID);
				}		
			}
		</script>
	</head>
	<body>
<?php
$ct=0;

if(isset($_GET['guestID'])){
	$guests = $_GET['guestID'];
	foreach($guests as $index => $guestID){
		foreach($_GET[$guestID] as $key => $propertyID){
			$ct++;
		}
	}

	// this means that there has been some kind of response
	if($ct>0){
		foreach($guests as $index => $guestID){
			wedding_guest::clearRSVP($guestID);
			foreach($_GET[$guestID] as $key => $propertyID){
				wedding_guest::respondRSVP($guestID, $propertyID);
			}
		}
	}

	if( isset($_GET['song']) ){
		$songName = $_GET['song'];
		// DO SANITATION ON SONG NAME HERE!!!!
		wedding_guest::clearSong($guests[0]);
		wedding_guest::saveSong($guests[0], $songName);
	}
}

?>

<style type='text/css'>
body{
	/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#fceabb+0,fccd4d+50,f8b500+51,fbdf93+100;Orange+3D+%235 */
	background: rgb(252,234,187); /* Old browsers */
	background: -moz-linear-gradient(top, rgba(252,234,187,1) 0%, rgba(252,205,77,1) 50%, rgba(248,181,0,1) 51%, rgba(251,223,147,1) 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(252,234,187,1)), color-stop(50%,rgba(252,205,77,1)), color-stop(51%,rgba(248,181,0,1)), color-stop(100%,rgba(251,223,147,1))); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, rgba(252,234,187,1) 0%,rgba(252,205,77,1) 50%,rgba(248,181,0,1) 51%,rgba(251,223,147,1) 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top, rgba(252,234,187,1) 0%,rgba(252,205,77,1) 50%,rgba(248,181,0,1) 51%,rgba(251,223,147,1) 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, rgba(252,234,187,1) 0%,rgba(252,205,77,1) 50%,rgba(248,181,0,1) 51%,rgba(251,223,147,1) 100%); /* IE10+ */
	background: linear-gradient(to bottom, rgba(252,234,187,1) 0%,rgba(252,205,77,1) 50%,rgba(248,181,0,1) 51%,rgba(251,223,147,1) 100%); /* W3C */
	font-family: "Trebuchet MS", Helvetica, sans-serif;
}

.myButton {
	-moz-box-shadow:inset 0px 1px 0px 0px #fbafe3;
	-webkit-box-shadow:inset 0px 1px 0px 0px #fbafe3;
	box-shadow:inset 0px 1px 0px 0px #fbafe3;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #ff5bb0), color-stop(1, #ef027d));
	background:-moz-linear-gradient(top, #ff5bb0 5%, #ef027d 100%);
	background:-webkit-linear-gradient(top, #ff5bb0 5%, #ef027d 100%);
	background:-o-linear-gradient(top, #ff5bb0 5%, #ef027d 100%);
	background:-ms-linear-gradient(top, #ff5bb0 5%, #ef027d 100%);
	background:linear-gradient(to bottom, #ff5bb0 5%, #ef027d 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff5bb0', endColorstr='#ef027d',GradientType=0);
	background-color:#ff5bb0;
	-moz-border-radius:6px;
	-webkit-border-radius:6px;
	border-radius:6px;
	border:1px solid #ee1eb5;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Arial;
	font-size:22px;
	font-weight:bold;
	padding:6px 24px;
	text-decoration:none;
	text-shadow:0px 1px 0px #c70067;
}
.myButton:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #ef027d), color-stop(1, #ff5bb0));
	background:-moz-linear-gradient(top, #ef027d 5%, #ff5bb0 100%);
	background:-webkit-linear-gradient(top, #ef027d 5%, #ff5bb0 100%);
	background:-o-linear-gradient(top, #ef027d 5%, #ff5bb0 100%);
	background:-ms-linear-gradient(top, #ef027d 5%, #ff5bb0 100%);
	background:linear-gradient(to bottom, #ef027d 5%, #ff5bb0 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ef027d', endColorstr='#ff5bb0',GradientType=0);
	background-color:#ef027d;
}
.myButton:active {
	position:relative;
	top:1px;
}

.heading{
	margin-left: 5px;
	margin-right: 5px;
	font-size:30px;
	color: #FFF;
	padding: 5px 5px 5px 5px;
	border-radius: 10px;
	margin-top: 5px;

	/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#ff00ff+0,ff82ff+100 */
	background: rgb(255,0,255); /* Old browsers */
	background: -moz-linear-gradient(top, rgba(255,0,255,1) 0%, rgba(255,130,255,1) 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,0,255,1)), color-stop(100%,rgba(255,130,255,1))); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, rgba(255,0,255,1) 0%,rgba(255,130,255,1) 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top, rgba(255,0,255,1) 0%,rgba(255,130,255,1) 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, rgba(255,0,255,1) 0%,rgba(255,130,255,1) 100%); /* IE10+ */
	background: linear-gradient(to bottom, rgba(255,0,255,1) 0%,rgba(255,130,255,1) 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff00ff', endColorstr='#ff82ff',GradientType=0 ); /* IE6-9 */

	-webkit-box-shadow: 10px 10px 9px 3px rgba(0,0,0,0.21);
	-moz-box-shadow: 10px 10px 9px 3px rgba(0,0,0,0.21);
	box-shadow: 10px 10px 9px 3px rgba(0,0,0,0.21);
}
.subheading{
	font-size: 20px;
	color: #111;
	border: 1px solid #000;
	padding: 5px 5px 5px 5px;
	border-radius: 10px;

	/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#eeeeee+0,cccccc+100;Gren+3D */
	background: rgb(238,238,238); /* Old browsers */
	background: -moz-linear-gradient(top, rgba(238,238,238,1) 0%, rgba(204,204,204,1) 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(238,238,238,1)), color-stop(100%,rgba(204,204,204,1))); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, rgba(238,238,238,1) 0%,rgba(204,204,204,1) 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top, rgba(238,238,238,1) 0%,rgba(204,204,204,1) 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, rgba(238,238,238,1) 0%,rgba(204,204,204,1) 100%); /* IE10+ */
	background: linear-gradient(to bottom, rgba(238,238,238,1) 0%,rgba(204,204,204,1) 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#eeeeee', endColorstr='#cccccc',GradientType=0 ); /* IE6-9 */
}
.midheading{
	margin-left: 5px;
	margin-right: 5px;
	margin-top: -15px;
	font-size:18px;
	font-weight: bold;
	color: #FFF;
	padding: 5px 5px 5px 5px;
	border-radius: 10px;

	/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#ff00ff+0,ff82ff+100 */
	background: rgb(255,0,255); /* Old browsers */
	background: -moz-linear-gradient(top, rgba(255,0,255,1) 0%, rgba(255,130,255,1) 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,0,255,1)), color-stop(100%,rgba(255,130,255,1))); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, rgba(255,0,255,1) 0%,rgba(255,130,255,1) 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top, rgba(255,0,255,1) 0%,rgba(255,130,255,1) 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, rgba(255,0,255,1) 0%,rgba(255,130,255,1) 100%); /* IE10+ */
	background: linear-gradient(to bottom, rgba(255,0,255,1) 0%,rgba(255,130,255,1) 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff00ff', endColorstr='#ff82ff',GradientType=0 ); /* IE6-9 */

	-webkit-box-shadow: 10px 10px 9px 3px rgba(0,0,0,0.21);
	-moz-box-shadow: 10px 10px 9px 3px rgba(0,0,0,0.21);
	box-shadow: 10px 10px 9px 3px rgba(0,0,0,0.21);
}
.smallheading{
	font-size: 14px;
	color: #000;
	border: 1px solid #000;
	padding: 5px 5px 5px 25px;
	border-radius: 10px;
	margin-left: 10px;

	/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#eeeeee+0,cccccc+100;Gren+3D */
	background: rgb(238,238,238); /* Old browsers */
	background: -moz-linear-gradient(top, rgba(238,238,238,1) 0%, rgba(204,204,204,1) 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(238,238,238,1)), color-stop(100%,rgba(204,204,204,1))); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, rgba(238,238,238,1) 0%,rgba(204,204,204,1) 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top, rgba(238,238,238,1) 0%,rgba(204,204,204,1) 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, rgba(238,238,238,1) 0%,rgba(204,204,204,1) 100%); /* IE10+ */
	background: linear-gradient(to bottom, rgba(238,238,238,1) 0%,rgba(204,204,204,1) 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#eeeeee', endColorstr='#cccccc',GradientType=0 ); /* IE6-9 */
}
.beOurDJ{

}
.content{
	font-size: 20px;
	color: #000;
	border: 1px solid #000;
	padding: 5px 5px 5px 5px;
	margin: 15px 15px 15px 15px;
	border-radius: 10px;
	background-color: #EFEFEF;
}
.subItem{
	border: 1px solid #000;
	padding: 5px 5px 5px 5px;
	border-radius: 10px;
	margin-top: 10px;
}
input {
    border-width: 2px;
    border-color: #cccccc;
    font-size: 22px;
    padding: 5px;
    border-radius: 10px;
    border-style: inset;
    box-shadow: inset 2px 3px 14px 0px rgba(42,42,42,.51);
}
li{
	margin-left: 20px;
	margin-bottom: 40px;
}

input[type='radio']{
    transform: scale(2);
    margin-right: 5px;
    margin-left: 10px;
}
input[type='checkbox']{
    transform: scale(2);
    margin-right: 5px;
    margin-left: 10px;
}
label{
	margin-left: 5px;
}

</style>

<div class='heading'>
	Welcome to <i class="fa fa-reply-all"></i> RSVPr
	<div class='subheading'>
		The Bizub Bhatnagar Wedding RSVP Tool
	</div>
</div>

<div class='content'>
<?php
if(isset($_GET['key']) && $_GET['key']!="" && $ct == 0){
	echo "<div class='simpleStyle'>";
	$guest = new wedding_guest('','','','','');
	$guest->getIDFromQR($_GET['key']);
	echo "<br>";
	$guest->getRSVPForm();
	echo "</div>";
} elseif( isset($_GET['key']) && $_GET['key']!= "" && $ct > 0 ) {
?>
	<p>Thank you for your response!</p>
	<p>Here are some next steps:</p>
	<ol>
		<li>
			Download WhatsApp for your smart device <a href='https://www.whatsapp.com/download/'>here.</a>  
			Once you have done so, you can send a message to 862-228-3280 to be added to the wedding group.  
			This will be used to share the latest updates, post media, etc in real time during the weekend of the wedding.
			<br><br>
			Whether you are attending or not, this will be a simple way to keep in touch with the bride and groom on this special weekend.
		</li>
		<li>
			If you wish to book a hotel near the wedding, please make sure to do so before September 16th to ensure that you get the group rate. Details can be found 
			<a href='http://www.dapcwiz.com/wedding/parallax_main.php#hotel_information'>here</a>.
		</li>
	</ol>	
<?php
} else {
?>

	<p>In order to successfully RSVP, you have several options:</p>
	<ol>
		<li>
			Use your smart device. <br>
			Get an app like <a href='https://play.google.com/store/apps/details?id=com.google.zxing.client.android&hl=en'>Barcode Scanner for Android <i class="fa fa-android"></i></a> 
			or <a href='https://itunes.apple.com/US/app/id474902001?mt=8'>RedLaser for IOS <i class="fa fa-apple"></i></a>.<br>
			Then, simply scan the QR code located on the bottom right of your invitation.<br>
			See the image below for help finding the QR Code.
		</li>
		<li>
			Continue on this website.<br>
			Find the 5 digit response key located directly under the QR Code on the bottom right of your invitation.<br>
			Type the key into the box below.<br>
			For help finding the response key, please refer to the image at the bottom of this page.<br>
			<form method='get' class='subItem'>
				<label>Response Key: </label> <input type='text' size='5' maxlength='5' name='key'> &nbsp;<input type='submit' value='continue to RSVP' class='MyButton'>
			</form>
		</li>
		<li>
			If all else fails and options 1 and 2 are not working for you,<br>
			feel free to email your response to <a href='mailto:laurenandarjun@gmail.com'>LaurenAndArjun@gmail.com</a>.
		</li>
	</ol>
	<p><img src="help-image.jpg" style='max-width: 95%;'></p>
<?php	
}
?>

</div>
	</body>
</html>