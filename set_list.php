<?php
require("phpqrcode/qrlib.php");
require("db-settings.php");
require("classes/qrcode.php");
require("classes/mailing_address.php");
require("classes/wedding_guest.php");

// check if form has been posted
if ($_SERVER['REQUEST_METHOD'] == 'POST'){

	// simple insecure password verification for some basic level of security
	$hash=md5($_POST['password']);

	//if($hash=="15b29ffdce66e10527a65bc6d71ad94d"){
	if(1 == 1){
		if ($_POST['address1']){
			// create new address object if address exists
			$address=new mailing_address($_POST['address1'],
				$_POST['address2'],
				$_POST['address3'],
				$_POST['address4'],
				$_POST['address5']
				);
			$addressID=$address->set_Address();

			// if address insert fails, check for existing address id
			if($addressID==0){
				$addressID=$address->get_AddressID();	
			}
		}

		// if an address ID has not been assigned, it it from the post object
		if (!isset($addressID)){
			$addressID=$_POST['groupWith'];
		}

	 	$guest = new wedding_guest($_POST['title'],
	 		$_POST['firstName'],
	 		$_POST['middleName'],
	 		$_POST['lastName'],
	 		$addressID
	 		);
	 	$guest->createQRcode();
	 	$result=$guest->set_guest();

	 	foreach ($_POST['invitation_properties'] as $selectedOption)
	 		$guest->setProperty($selectedOption);

	 	foreach ($_POST['general_properties'] as $selectedOption)
	 		$guest->setProperty($selectedOption);

	 	if($result==0){
	 		echo "error :(";
	 	}else{
	 		echo "<h3>success!</h3>";
	 	}
	 	//$guest->firstName=$_POST['firstName'];
	}else{
		echo "invalid password<br><br>";
	}
}

// html form to populate data
?>
<html>
	<head>
		<style type="text/css">
		label {
			width: 250px;
			font-weight: bold;
			display:inline-block;
		}
		</style>
	</head>

	<body>

	<p>
		<h1>Create New Guest</h1>
	</p>

	<form method="post">
		<label>Title </label> 
<?php
wedding_guest::getTitles('');
?>
		<br><br>
		<label>First Name</label> <input type="text" name="firstName"><br><br>
		<label>Middle Name</label> <input type="text" name="middleName"><br><br>
		<label>Last Name</label> <input type="text" name="lastName"><br><br>
		<br>

		invited to:<br>
<?php
wedding_guest::getInvitationProperties('invitation');
?>
		<br>properties of guest:<br>
<?php
wedding_guest::getInvitationProperties('general');
?>		

		<div style="border: 1px solid black; padding: 5px 5px 5px 5px;">
			<label>Address 1</label> <input type="text" name="address1"><br><br>
			<label>Address 2</label> <input type="text" name="address2"><br><br>
			<label>Address 3</label> <input type="text" name="address3"><br><br>
			<label>Address 4</label> <input type="text" name="address4"><br><br>
			<label>Address 5</label> <input type="text" name="address5"><br><br>
			<h3>OR</h3>
			<label>Group with</label>
			<select name="groupWith">
				<option value="unique"></option>

<?php
wedding_guest::getAllGuestAddresses();
?>
			</select>
		</div><br><br>
		<!--Password: <input type="password" name="password"><br><br>-->

		<input type="submit">
	</form>

	</body>
</html>